// VARIABLES
// MAIN
const main = document.querySelector('.main')

// BUTTON
// BUTTON > DISK
const playButton = document.querySelector('.js-play-button')
const pauseButton = document.querySelector('.js-pause-button')
const prevButton = document.querySelector('.js-prev-button')
const nextButton = document.querySelector('.js-next-button')
// BUTTON > BOARD
const welcomeButton = document.querySelectorAll('.board__welcome__button')
const sizeButton = document.querySelectorAll('.board__size__button')
const flavorButton = document.querySelectorAll('.board__flavor__button')
const toppingButton = document.querySelectorAll('.board__topping__button')
const strawButton = document.querySelectorAll('.board__straw__button')
const labelButton = document.querySelectorAll('.board__label__button')
// let activeButton = 0
//DISK
const disk = document.querySelector('.main__right__bot__player__box__disk')
// let open = new Audio ('../audio/open.wav');
//disk music
// const earfQuakeSound = new Audio('audio/earfquake.mp3');
const playlist = [
    new Audio("/audio/bossa.mp3"),
    new Audio("/audio/dreams.mp3"),
    new Audio("/audio/softly.mp3"),
    new Audio("/audio/earfquake.mp3"),
    new Audio("/audio/lanterns.mp3"),
    new Audio("/audio/technicolor.mp3")
]
let playlistStateIndex = 0
//DISK MUSIC PLAY
//play

playButton.addEventListener('click', () => {
    disk.classList.add('disk-animation')
    playlist[playlistStateIndex].play()
    pauseButton.classList.remove('hidden')
    playButton.classList.add('hidden')
})
//pause
pauseButton.addEventListener('click', () => {
    disk.classList.remove('disk-animation')
    playlist[playlistStateIndex].pause()
    pauseButton.classList.add('hidden')
    playButton.classList.remove('hidden')
})
//next
nextButton.addEventListener("click", () => {
    disk.classList.add('disk-animation')
    pauseButton.classList.remove('hidden')
    playButton.classList.add('hidden')
    playlist[playlistStateIndex].currentTime = 0
    playlist[playlistStateIndex].pause()
    playlistStateIndex = (playlistStateIndex + 1) % playlist.length
    playlist[playlistStateIndex].play()
})
//prev
prevButton.addEventListener("click", () => {
    disk.classList.add("disk-animation")
    pauseButton.classList.remove("hidden")
    playButton.classList.add("hidden")
    playlist[playlistStateIndex].currentTime = 0
    playlist[playlistStateIndex].pause()
    if (playlistStateIndex === 0) {
        playlistStateIndex = playlist.length - 1
    } else {
        playlistStateIndex--
    }
    playlist[playlistStateIndex].play()
})
//autoplay
playlist.forEach((sound) => {
    sound.addEventListener("ended", () => {
        playlistStateIndex = (playlistStateIndex + 1) % playlist.length
        playlist[playlistStateIndex].play()
    })
})

// // CLOCK
// const tick = () => {
//     const date = new Date()
//     const hours = date.getHours()
//     const minutes = date.getMinutes()
//     if(minutes < 10 && hours < 10){
//         document.querySelector("h3.js-clock").innerText = `0${hours} : 0${minutes}`
//     }else if(minutes < 10 && hours > 10){
//         document.querySelector("h3.js-clock").innerText = `${hours} : 0${minutes}`
//     }else if(minutes > 10 && hours < 10){
//         document.querySelector("h3.js-clock").innerText = `0${hours} : ${minutes}`
//     }else{
//         document.querySelector("h3.js-clock").innerText = `${hours} : ${minutes}`
//     }
// }
// window.setInterval(tick, 1000)
