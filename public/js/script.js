// On va chercher la balise link
let themeLink = document.getElementById("theme-link")

// On utilise le localStorage pour garder l'infos choisis par l'utilisateur
// Si il y a un thème stocké dans le localStorage
// thème clair choisi par défault
// style_"nom du thème"
if (localStorage.theme != null) {
    themeLink.href = `/css/style_${localStorage.theme}.css`
    // Si il n'y a pas de thème stocké
} else {
    themeLink.href = "/css/style_clair.css"
    localStorage.theme = "clair"
}
// Ecouteur d'évènement "click" sur span
// On utilise une fonction anonyme pour accéder au this
document.getElementById("theme").addEventListener("click", function () {
    // Si le thème sombre est choisi, le texte change en clair
    if (localStorage.theme == "clair") {
        localStorage.theme = "sombre"
        this.innerHTML = 
        `<i class="far fa-moon"></i>
        <button class="btn text-light mx-2">
        <i class="fas fa-toggle-off"></i>
        </button>
        <i class="far fa-sun"></i>`
        // Si le thème clair est choisi, le texte change en sombre
    } else {
        localStorage.theme = "clair"
        this.innerHTML = 
        `<i class="far fa-moon"></i>
        <button class="btn text-light mx-2">
        <i class="fas fa-toggle-on"></i>
        </button>
        <i class="far fa-sun"></i>`
    }
    themeLink.href = `/css/style_${localStorage.theme}.css`
})

// Variables globales
let lastId = 0 // id du dernier message affiché

// On attend le chargement du document
window.onload = () => {
    // On va chercher la zone texte
    let texte = document.querySelector("#texte")
    // On choisi "keyup" pour être sur qu'on  appuyé et relâcher
    texte.addEventListener("keyup", verifEntree)

    // On va chercher le bouton valid
    let valid = document.querySelector("#valid")
    valid.addEventListener("click", ajoutMessage)

    // On charge les nouveaux messages
    setInterval(chargeMessages, 1000)
}

/**
 * Charge les derniers messages en Ajax et les insère dans la discussion
 * On le fait en Ajax pour ne pas avoir à recharger la page
 */
function chargeMessages() {
    // On instancie XMLHttpRequest
    let xmlhttp = new XMLHttpRequest()

    // On gère la réponse
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {

                // On a une réponse
                // On convertit la réponse en objet JS
                let messages = JSON.parse(this.response)
                messages = JSON.parse(messages)

                // On retourne l'objet
                messages.reverse()

                // On récupère la div #discussion
                let discussion = document.querySelector("#discussion")
                discussion.innerHTML = '';
                for (let message of messages) {
                    // On transforme la date du message en JS
                    // let dateMessage = new Date(message[0]['created_at']);
                    // console.log(message[0]['created_at']['timestamp']);
                    // Mis en place de la date et l'heure de création du message
                    let dateMessage = new Date(message[0]['created_at']['timestamp'] * 1000);

                    // On ajoute le contenu avant le contenu actuel de discussion
                    discussion.innerHTML = `<div class="mb-1">
                    <img style="border-radius:50% !important; width:30px; height:30px; object-fit: cover; display:inline-block; margin-right:10px; vertical-align:middle;" src="/images/avatars/${message.avatar}" alt="${message.avatar}" />
                    <strong><p style="display:inline-block; vertical-align:middle;width:calc(100% - 50px); border-top:;">${message.username}</strong>
                    a écrit le ${dateMessage.toLocaleString()} : 
                    ${message[0]['message']}</p></div>` + discussion.innerHTML
                    // Retour au dernier message envoyé
                    discussion.scrollTop = discussion.scrollHeight;

                    // On met à jour le lastId
                    lastId = message.id
                }
            } else {
                // On gère les erreurs
                let erreur = JSON.parse(this.response)
                alert(erreur.message)
            }
        }
    }

    // On ouvre la requête
    xmlhttp.open("GET", "/chat/charge?lastId=" + lastId)
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest')

    // On envoie
    xmlhttp.send()
}


/**
 * Cette fonction vérifie si on a appuyé sur la touche entrée
 */
function verifEntree(e) {
    if (e.key == "Enter") {
        ajoutMessage();
    }
}

/**
 * Cette fonction envoie le message en ajax à un fichier ajoutMessage.php
 */
function ajoutMessage() {
    // On récupère le message
    let message = document.querySelector("#texte").value
    // On vérifie si le message n'est pas vide
    if (message != "") {
        // On crée un objet JS, un objet "message" de proprité la variable message
        let donnees = {}
        donnees["message"] = message

        // On convertit les données en JSON
        let donneesJson = JSON.stringify(donnees)

        // On envoie les données en POST en Ajax
        // On instancie XMLHttpRequest
        let xmlhttp = new XMLHttpRequest()

        // On gère la réponse
        xmlhttp.onreadystatechange = function () {
            // On vérifie si la requête est terminée
            // "this" = xmlhttp
            // "readyState" composé de 4 étapes de traitement de la requête
            if (this.readyState == 4) {
                // On vérifie qu'on reçoit un code 201
                // 201 = Requête traitée avec succès et création d'un document
                if (this.status == 201) {
                    // L'enregistrement a fonctionné
                    // On efface le champ texte
                    // en utilisant un écouteur d'evnmt sur l'id texte 
                    // et on remplace sa valeur par ""
                    document.querySelector("#texte").value = ""
                } else {
                    // L'enregistrement a échoué
                    let reponse = JSON.parse(this.response)
                    alert(reponse.message)
                }
            }
        }

        // On ouvre la requête
        xmlhttp.open("POST", "/chat/add")
        xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
        // On envoie la requête en incluant les données
        xmlhttp.send(donneesJson)
    }
}

// MODAL DU CHAT
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}