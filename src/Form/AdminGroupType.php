<?php

namespace App\Form;

use App\Entity\Group;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;


class AdminGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Nom du groupe :',
                'constraints' => [
                    new NotBlank([
                        'message' => 'La saisi d\'un nom est obligatoire !',
                    ])
                ]
            ])
            ->add('color', ColorType::class, [
                'label' => 'Couleur :',
            ])
            ->add('user', EntityType::class, [
                // looks for choices from this entity
                'class' => User::class,
                'label' => 'Membres :',
                // uses the User.username property as the visible option string
                'choice_label' => 'username',
                // used to render a select box, check boxes or radios
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Group::class,
        ]);
    }
}
