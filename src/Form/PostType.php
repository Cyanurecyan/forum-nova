<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (in_array('ROLE_ADMIN', $options['role'])) {
            // do as you want if admin
            $builder 
                ->add('priority', ChoiceType::class, [
                    'required' => false,
                    'label' => 'Priorité :',
                    'choices' => [
                        'Normale' => null,
                        'Administrateur' => true,
                    ]
                ])
                ->add('title', Texttype::class, [
                    'required' => false,
                    'label' => 'Titre :',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Veuillez entrer un titre :',
                            'groups' => ['new_post']
                        ])
                    ]
                ])
                ->add('content', CKEditorType::class, [
                    'required' => true,
                    'label' => false,
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Votre message ne peut pas être vide.',
                            'groups' => ['new_post', 'add_post']
                        ])
                    ]
                ])
                ->add('categorie', EntityType::class, [
                    'required' => true,
                    'label' => 'Catégorie :',
                    'class' => Categorie::class,
                    'choice_label' => 'name',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'La catégorie doit être définie.',
                            'groups' => ['new_post']
                        ])
                    ],
                ])
            ;
        } else {
            $builder
                ->add('title', Texttype::class, [
                    'required' => false,
                    'label' => 'Titre :',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Veuillez entrer un titre :',
                            'groups' => ['new_post']
                        ])
                    ]
                ])
                ->add('content', CKEditorType::class, [
                    'required' => true,
                    'label' => false,
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Votre message ne peut pas être vide.',
                            'groups' => ['new_post', 'add_post']
                        ])
                    ]
                ])
                ->add('categorie', EntityType::class, [
                    'required' => true,
                    'label' => 'Catégorie :',
                    'class' => Categorie::class,
                    'choice_label' => 'name',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'La catégorie doit être définie.',
                            'groups' => ['new_post']
                        ])
                    ],
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
            'role' => ['ROLE_ADMIN', 'ROLE_USER']
        ]);
    }
}
