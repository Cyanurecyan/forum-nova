<?php

namespace App\Form;

use App\Entity\Post;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;



class AdminPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false,
                'label' => 'Titre :',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir un titre pour votre Topic',
                    ])
                ]
            ])
            // Génère un menu déroulant contenant les données de la table "categorie"
            ->add('categorie', EntityType::class, [
                'required' => true,
                'label' => 'Catégorie du sujet :',
                'class' => Categorie::class,
                'choice_label' => 'name'
            ])
            ->add('priority', ChoiceType::class, [
                'required' => true,
                'label' => 'Priorité :',
                'choices' => [
                    'Classique' => false,
                    'Administrateur' => true
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Post::class,
        ]);
    }
}
