<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder           
            ->add('username', TextType::class, [
                'required' => true,
                'label' => 'Nom',
                'constraints' => [
                    new NotBlank([
                        'message' => 'La saisie du nom est obligatoire',
                    ]),
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Adresse e-mail',
                'constraints' => [
                    new NotBlank([
                        'message' => 'La saisie d\'un e-mail est obligatoire',
                    ]),
                    new Email([
                        'message' => 'L\'adresse e-mail est invalide',
                    ]),
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Avatar',
                // Options de VichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'imagine_pattern' => "profil",
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir un avatar',
                    ]),
                    new Image([
                        'maxSize' => '3M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 3Mo.',
                        'mimeTypes' => ['image/gif', 'image/png', 'image/jpeg', 'image/webp'],
                        'mimeTypesMessage' => 'Cette image est invalide.',

                    ])
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
