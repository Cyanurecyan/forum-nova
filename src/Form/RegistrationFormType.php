<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\Image;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Email :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Votre email doit être renseigné.',
                        'groups' => ['new_profil']
                    ]),
                    new Email([
                        'message' => 'Votre email doit être au format monmail@mail.com'
                    ])
                ]
            ])
            ->add('username', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Nom d\'utilisateur :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre nom d\'utilisateur.',
                        'groups' => ['new_profil', 'edition_profil']
                    ])
                ]
            ])
            ->add('firstname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Prénom :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre prénom.',
                        'groups' => ['new_profil']
                        
                    ])
                ]
            ])
            ->add('lastname', TextType::class, [
                'required' => true, // champs obligatoires
                'label' => 'Nom de famille :', // label du champ
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez renseigner votre nom.',
                        'groups' => ['new_profil']

                    ])
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'required' => true, // champs obligatoires
                'mapped' => false,
                'label' => 'J\'ai lu et j\'accepte les conditions générales d\'utilisation et la politique d\'utilisation des données.', // label du champ
                'constraints' => [
                    new IsTrue([
                        'message' => 'Obligatoire',
                        'groups' => ['new_profil']

                    ]),
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas.',
                'first_options' => ['label' => 'Mot de passe :'],
                'second_options' => ['label' => 'Confirmer le mot de passe :'],
                'required' => true,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Renseignez un mot de passe.',
                        'groups' => ['new_profil']

                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit faire au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Avatar :',
                // Options de VichImageType
                'allow_delete' => false,
                'download_uri' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez choisir un avatar',
                        'groups' => ['new_profil']

                    ]),
                    new Image([
                        'maxSize' => '3M',
                        'maxSizeMessage' => 'Le fichier ne doit pas dépasser les 3Mo.',
                        'mimeTypes' => ['image/gif', 'image/png', 'image/jpeg', 'image/webp'],
                        'mimeTypesMessage' => 'Cette image est invalide.',
                        'groups' => ['new_profil', 'edition_profil']

                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
