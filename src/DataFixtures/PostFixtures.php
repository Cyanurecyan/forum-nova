<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

use App\Entity\Post;
use Faker;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    
    /**
     * Permet de dire à notre fixtures si elle dépend d'autres fixtures
     */
    public function getDependencies() 
    {
        return [
            CategorieFixtures::class,
            UserFixtures::class
        ];
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for() pour choisir le nombre d'éléments mis en BDD
        for ($i=0; $i <= 50; $i++) { 
            $post = new Post();

            $categorie = $this->getReference('categorie_'. random_int(0, 10));
            $user = $this->getReference('user_' . random_int(0,10));

            // Utilisation des setters
            $post->setContent($faker->text(500));
            $post->setUser($user); 
            $post->setTitle($faker->sentence());
            $post->setPriority(false);
            $post->setCategorie($categorie);
            $post->setCreatedAt($faker->dateTimeBetween('- 4 years'));

            $this->addReference('post_'.$i, $post);

            // Garde de côté les données en attendant l'exécution des requêtes
            $manager->persist($post);
        }

        // Création d'une boucle for() pour choisir le nombre d'éléments mis en BDD
        for ($i=0; $i <= 200; $i++) { 
            $post = new Post();

            $parent = $this->getReference('post_' . random_int(0,50));
            $user = $this->getReference('user_' . random_int(0,10));

            // Utilisation des setters
            $post->setContent($faker->text(500));
            $post->setUser($user); 
            $post->setIdParent($parent);
            $post->setCreatedAt($faker->dateTimeBetween('- 4 years'));

            // Garde de côté les données en attendant l'exécution des requêtes
            $manager->persist($post);
        }

        $manager->flush();
    }
}
