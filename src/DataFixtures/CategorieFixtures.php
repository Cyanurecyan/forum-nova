<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Categorie;
use Faker;

class CategorieFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        // Création d'une boucle for() pour choisir le nombre d'éléments mis en BDD
        for ($i=0; $i <= 10; $i++) { 
            $category = new Categorie();

            // Utilisation des setters
            $category->setName($faker->colorName);
            $category->setCreatedAt($faker->dateTimeBetween('- 4 years'));

            // Enregistre l'objet dans une référence. 
            // Cad qu'on pourra utiliser les objets enregistrés dans une autre fixture 
            // afin de pouvoir effectuer des relations de table.
            // le premier paramètre est un nom qui se doit d'être unique.
            // le second paramètre est l'objet qui sera lié à ce nom.
            $this->addReference('categorie_'.$i, $category);

            // Garde de côté les données en attendant l'exécution des requêtes
            $manager->persist($category);
        }

        $manager->flush();
    }
}
