<?php

namespace App\Controller;

use App\Entity\Group;
use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends AbstractController
{
   /**
    * @Route("/group/{id}", name="group_page")
    */
   public function index(PaginatorInterface $paginator, Request $request, $id): Response
   {
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
      $group = $this->getDoctrine()->getRepository(Group::class)->find($id);
      
      return $this->render('group/index.html.twig', [
         'group' => $group,
         'categories' => $categories
      ]);
   }
}