<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Post;
use App\Form\ProfilType;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfilController extends AbstractController
{
    /**
     * @Route("/profil/{id}", name="profil")
     */
    public function profil($id): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        
        
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $post = $this->getDoctrine()->getRepository(Post::class)->findBy([
            'user' => $user,
        ]);

        if(!$user) {
            throw $this->createNotFoundException('L\'utilisateur n\'existe pas');
        }

        return $this->render('profil/index.html.twig', [
            'categories' => $categories,
            'user' => $user,
            'post' => $post,
        ]);
    }

    /**
     * @Route("/profil/editer/{id}", name="profil_editer")
     */
    public function edit($id, Request $request): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $formEditUser = $this->createForm(ProfilType::class, $user);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {
            // obtenir la date de màj
            $user->setUpdatedAt(new \DateTimeImmutable());
            $user->getId();
            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Modifications enregistrées !');

            // redirection
            return $this->redirectToRoute('profil', [
                'id' => $id
            ]);
        }

        return $this->render('profil/edit.html.twig', [
            'formEditUser' => $formEditUser->createView(),
            'categories' => $categories
        ]);
    }
   
}
