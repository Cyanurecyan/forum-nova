<?php

namespace App\Controller\Admin;

use App\Entity\Group;
use App\Entity\Categorie;
use App\Form\AdminGroupType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GroupController extends AbstractController
{
   /**
    * @Route("/admin/group", name="admin_groups")
    */
   public function index(PaginatorInterface $paginator, Request $request): Response
   {
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
      $query = $this->getDoctrine()->getRepository(Group::class)->findAll();

      $page = $request->query->getInt('page', 1);
      $groups = $paginator->paginate(
         $query,
         $page === 0 ? 1 : $page,
         10
      );

      return $this->render('admin/group/index.html.twig', [
         'categories' => $categories,
         'groups' => $groups
      ]);
   }

   /**
    * @Route("/admin/group/new", name="admin_groups_new")
    */
   public function new(Request $request)
   {
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
      $query = $this->getDoctrine()->getRepository(Group::class)->findAll();

      // Instancie l'entité "Picture"
      $group = new Group();

      // Premier paramètre : Le formulaire dont ont a besoin
      // Deuxième paramètre : l'objet de l'entité à vide
      $formNewGroup = $this->createForm(AdminGroupType::class, $group);
      $formNewGroup->handleRequest($request);

      // Vérifie si le formulaire est envoyé et valide
      if ($formNewGroup->isSubmitted() && $formNewGroup->isValid()) {

         $group->setCreatedAt(new \DateTimeImmutable());

         // insertion en BDD
         $doctrine = $this->getDoctrine()->getManager();
         $doctrine->persist($group);
         $doctrine->flush();

         // créer un message flash
         $this->addFlash('success', 'Nouveau groupe ajouté !');

         // redirection
         return $this->redirectToRoute('admin_groups');
      }

      return $this->render('admin/group/new.html.twig', [
         'formNewGroup' => $formNewGroup->createView(),
         'categories' => $categories,
      ]);
   }

   /**
    * @Route("/admin/group/edit/{id}", name="admin_groups_edit")
    */
   public function edit($id, Request $request)
   {
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
      
      // selection en BDD
      $group = $this->getDoctrine()->getRepository(Group::class)->find($id);

      if (!$group) {
         throw $this->createForm(AdminGroupType::class, $group);
      }

      $formEditGroup = $this->createForm(AdminGroupType::class, $group);
      $formEditGroup->handleRequest($request);

      if ($formEditGroup->isSubmitted() && $formEditGroup->isValid()) {

         $group->setUpdatedAt(new \DateTimeImmutable());

         // insertion en BDD
         $doctrine = $this->getDoctrine()->getManager();
         $doctrine->persist($group);
         $doctrine->flush();

         // créer un message flash
         $this->addFlash('success', 'Modifications enregistrées !');

         // redirection
         return $this->redirectToRoute('admin_groups');
      }

      return $this->render('admin/group/edit.html.twig', [
         'formEditCategorie' => $formEditGroup->createView(),
         'categories' => $categories
      ]);
   }

   /**
    * @Route("/admin/group/delete/{id}", name="admin_groups_delete")
    */
   
   public function delete($id)
   {
      $group = $this->getDoctrine()->getRepository(Group::class)->find($id);

      if (!$group) {
         throw $this->createNotFoundException('La catégorie n\'existe pas');
      }

      $doctrine = $this->getDoctrine()->getManager();
      $doctrine->remove($group);
      $doctrine->flush();

      $this->addFlash('success', 'Le groupe a bien été supprimé.');

      return $this->redirectToRoute('admin_groups');
   }

}