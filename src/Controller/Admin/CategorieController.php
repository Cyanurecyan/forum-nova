<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use App\Form\AdminCategorieType;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategorieController extends AbstractController
{
    /**
     * @Route("/admin/categories", name="admin_categories")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $query = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $categories = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            20
        );

        return $this->render('admin/categorie/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/new", name="admin_categories_new")
     */
    public function new(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        // Instancie l'entité "Picture"
        $categorie = new Categorie();

        // Premier paramètre : Le formulaire dont ont a besoin
        // Deuxième paramètre : l'objet de l'entité à vide
        $formNewCategorie = $this->createForm(AdminCategorieType::class, $categorie);
        $formNewCategorie->handleRequest($request);

        // Vérifie si le formulaire est envoyé et valide
        if ($formNewCategorie->isSubmitted() && $formNewCategorie->isValid()) {

            $categorie->setCreatedAt(new \DateTimeImmutable());

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($categorie);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Nouvelle catégorie ajoutée !');

            // redirection
            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/categorie/new.html.twig', [
            'formNewCategorie' => $formNewCategorie->createView(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/edit/{id}", name="admin_categories_edit")
     */
    public function edit($id, Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        
        // selection en BDD
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        if (!$categorie) {
            throw $this->createForm(AdminCategorieType::class, $categorie);
        }

        $formEditCategorie = $this->createForm(AdminCategorieType::class, $categorie);
        $formEditCategorie->handleRequest($request);

        if ($formEditCategorie->isSubmitted() && $formEditCategorie->isValid()) {

            $categorie->setUpdatedAt(new \DateTimeImmutable());

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($categorie);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Modifications enregistrées !');

            // redirection
            return $this->redirectToRoute('admin_categories');
        }

        return $this->render('admin/categorie/edit.html.twig', [
            'formEditCategorie' => $formEditCategorie->createView(),
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categories/delete/{id}", name="admin_categories_delete")
     */
    
    public function delete($id)
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->find($id);

        if (!$categorie) {
            throw $this->createNotFoundException('La catégorie n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($categorie);
        $doctrine->flush();

        $this->addFlash('success', 'La catégorie a bien été supprimée');

        return $this->redirectToRoute('admin_categories');
    }

}