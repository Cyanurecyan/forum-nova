<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Categorie;
use App\Form\AdminUserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    /**
     * @Route("/admin/users", name="admin_users")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $query = $this->getDoctrine()->getRepository(User::class)->findAll();
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $page = $request->query->getInt('page', 1);
        $users = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            25
        );

        return $this->render('admin/user/index.html.twig', [
            'users' => $users,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/users/delete/{id}", name="admin_users_delete")
     */
    public function delete(User $user)
    {
        if(!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($user);
        $doctrine->flush();

        $this->addFlash('success', 'L\'utilisateur a bien été supprimé');

        return $this->redirectToRoute('admin_users');
    }
    
    /**
     * @Route("/admin/users/edit/{id}", name="admin_users_edit")
     */
    public function edit($id, Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        if(!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        $formEditUser = $this->createForm(AdminUserType::class, $user);

        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted() && $formEditUser->isValid()) {

            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($user);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'L\'utilisateur a bien été modifié');

            // redirection
            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/user/edit.html.twig', [
            'formEditUser' => $formEditUser->createView(),
            'categories' => $categories
        ]);
    }
}