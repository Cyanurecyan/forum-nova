<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use App\Entity\Categorie;
use App\Form\AdminPostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class PostController extends AbstractController
{
    /**
     * @Route("/admin/posts", name="admin_posts")
     */
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $query = $this->getDoctrine()->getRepository(Post::class)->findby([
            'id_parent' => null
        ]);

        $page = $request->query->getInt('page', 1);
        $posts = $paginator->paginate(
            $query,
            $page === 0 ? 1 : $page,
            25
        );

        return $this->render('admin/post/index.html.twig', [
            'posts' => $posts,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/posts/delete/{id}", name="admin_posts_delete")
     */
    public function delete($id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        
        if(!$post) {
            throw $this->createNotFoundException('Le post n\'existe pas');
        }

        $doctrine = $this->getDoctrine()->getManager();
        $doctrine->remove($post);
        $doctrine->flush();

        $this->addFlash('success', 'Le post a bien été supprimé');

        return $this->redirectToRoute('admin_posts');
    }

    /**
     * @Route("/admin/posts/edit/{id}", name="admin_posts_edit")
     */
    public function edit($id, Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        
        if(!$post) {
            throw $this->createNotFoundException('Le post n\'existe pas');
        }

        $formEditPost = $this->createForm(AdminPostType::class, $post);
        $formEditPost->handleRequest($request);

        if ($formEditPost->isSubmitted() && $formEditPost->isValid()) {

            $post->setUpdatedAt(new \DateTimeImmutable());
            // insertion en BDD
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($post);
            $doctrine->flush();

            // créer un message flash
            $this->addFlash('success', 'Le post a bien été modifié');

            // redirection
            return $this->redirectToRoute('admin_posts');
        }

        return $this->render('admin/post/edit.html.twig', [
            'formEditPost' => $formEditPost->createView(),
            'categories' => $categories
        ]);

    }
}
