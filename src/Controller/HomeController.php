<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Categorie;
use App\Entity\Post;
use App\Entity\User;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, PaginatorInterface $paginator): Response
    {

        $query = $this->getDoctrine()->getManager();

        // Récupération des données des catégories, de leurs sujets, de leurs auteurs
        $categories = $query->getRepository(Categorie::class)->findAll();

        // Récupération des topics dans la catégorie, pour être paginés
        $rTopics = $query->getRepository(Post::class)->findBy([
            "id_parent" => null
        ], [
            "created_at" => "DESC"
        ]);
        $page = $request->query->getInt('page', 1);
        $topics = $paginator->paginate(
            $rTopics, // Requête contenant les données à paginer (ici nos articles)
            $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            3 // Nombre de résultats par page
        );

        // récupération du nombre de sujets, de posts et de membres
        $rPost = $query->getRepository(Post::class);
        $nbPost = $rPost->createQueryBuilder('a')->select('count(a.id)')->getQuery()->getSingleScalarResult();
        $rUser = $query->getRepository(User::class);
        $nbUser = $rUser->createQueryBuilder('a')->select('count(a.id)')->getQuery()->getSingleScalarResult();

        // récupération du dernier membre
        $newMember = $query->getRepository(User::class)->findOneBy([], ['id' => 'DESC']);

        // informations retournées dans la vue
        return $this->render('home/index.html.twig', [
            'categories' => $categories,
            'topics' => $topics,
            'nbpost' => $nbPost,
            'nbuser' => $nbUser,
            'newmember' => $newMember
        ]);
        
        
    }

}
