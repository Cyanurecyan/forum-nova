<?php

namespace App\Controller;

use App\Entity\Chat;
use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


class ChatController extends AbstractController
{
    /**
     * @Route("/chat", name="chat")
     */
    public function chat(): Response
    {
        
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        return $this->render('chat/index.html.twig', [
            'categories' => $categories
        ]);
    }
    /**
     * @Route("/chat/add", name="chat_add")
     */
    public function addMessage(Request $request)
    {
        // On vérifie si on a une requête XMLHttpRequest
        if ($request->isXmlHttpRequest()) {
            // On vérifie les données après les avoirs décodées
            $donnees = json_decode($request->getContent());

            // On instancie un nouveau message
            $messages = new Chat();

            // On hydrate nos messages 
            $messages->setMessage($donnees->message);
            $messages->setCreatedAt(new \DateTime());
            $messages->setUser($this->getUser());

            // On sauvegarde en bdd
            // $em = entity manager
            $em = $this->getDoctrine()->getManager();
            $em->persist($messages);
            $em->flush();

            // On retourne la confirmation
            return new Response('Ok', 201);
        }
        return new Response('Erreur', 404);
    }
    /**
     * @Route("/chat/charge", name="chat_charge", methods={"GET"})
     */
    public function chargeMessage()
    {
        $chat = $this->getDoctrine()->getRepository(Chat::class)->chatFindAll();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($chat, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);
        
        $response = new JsonResponse($jsonContent);

        // $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
}
