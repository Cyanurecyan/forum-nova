<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Post;
use App\Entity\Categorie;

use App\Form\PostType;

class PostController extends AbstractController
{
   /**
    * @Route("/topic/{id}", name="page_topic")
    */
   public function index($id, Request $request, PaginatorInterface $paginator): Response
   {
      
      $query = $this->getDoctrine()->getManager();
      $categories = $query->getRepository(Categorie::class)->findAll();

      // Récupération des données du topic visé
      $topic = $query->getRepository(Post::class)->findOneBy([
         'id' => $id
      ]);
      
      // Récupération des posts dans le topic, pour être paginés
      $rPosts = $query->getRepository(Post::class)->findBy([
         'id_parent' => $id,
      ], [
         'created_at' => 'ASC'
      ]);

      $page = $request->query->getInt('page', 1);
      $posts = $paginator->paginate(
         $rPosts, // Requête contenant les données à paginer (ici nos articles)
         $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
         10 // Nombre de résultats par page
      );
      

      // méthode d'ajout via le formulaire
      $post = new Post();
      $formContent = $this->createForm(PostType::class, $post, [
         // 'role' => $this->getUser()->getRoles(),
         'validation_groups' => 'add_post'
      ]);
      $formContent->handleRequest($request);
         // dd($formContent);
      // Vérifie si le formulaire est envoyé et valide :
      if($formContent->isSubmitted() && $formContent->isValid()) {
         $post->setCreatedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));
         $post->setUser($this->getUser());
         $post->setIdParent($topic);
         $topic->setUpdatedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));

         // Insertion en bdd
         $query->persist($post);
         $query->flush();

         // Création d'un message flash
         $this->addFlash('success', 'Votre message a été envoyé.');

         // Redirection vers la page d'accueil
         return $this->redirectToRoute('page_topic', [
            'id' => $id
         ]);
      }

      // informations retournées dans la vue
      return $this->render('topic/index.html.twig',[
         'formContent' => $formContent->createView(),
         'categories' => $categories,
         'topic' => $topic,
         'posts' => $posts
      ]);
   }

   /**
   * @Route("/topic/edit/{id}", name="edit_post")
   */
   public function editPost($id, Request $request): Response
   {
      $query = $this->getDoctrine()->getManager();
      $categories = $query->getRepository(Categorie::class)->findAll();

      $post = new Post();
      $post = $query->getRepository(Post::class)->find($id);

      if(!$post) {
         throw $this->createNotFoundException('Ce post n\'existe pas.');
      }

      // si le sujet n'a pas de parent, alors il est lui même parent
      // il faudra donc utiliser le validation group d'ajout d'un nouveau sujet
      if ($post->getIdParent() == null) { 
         // Premier paramètre : le formulaire dont on a besoin
         // Deuxième paramètre : l'objet de l'entité à vide
         $postEdit = $this->createForm(PostType::class, $post, [
            'role' => $this->getUser()->getRoles(),
            'validation_groups' => 'new_post'
         ]);
         $postEdit->handleRequest($request);

         // Vérifie si le formulaire est envoyé et valide :
         if($postEdit->isSubmitted() && $postEdit->isValid()) {
            $post->setUpdatedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));

            // Insertion en bdd
            $query->persist($post);
            $query->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Modification effectuée.');

            $topic = $post->getIdParent();
            // si le topic a un parent, il est donc un enfant, on récupère l'id du parent pour la redirection
            if($topic) {
               $idtopic = $topic->getId();
            } else { // sinon, on récupère simplement l'id du post en lui-même
               $idtopic = $id;
            }

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('page_topic', [
               'id' => $idtopic
            ]);
         }

         // informations retournées dans la vue
         return $this->render('topic/edit_topic.html.twig',[
            'formContent' => $postEdit->createView(),
            'categories' => $categories
         ]);
         
      } else {
         // Premier paramètre : le formulaire dont on a besoin
         // Deuxième paramètre : l'objet de l'entité à vide
         $postEdit = $this->createForm(PostType::class, $post, [
            'role' => $this->getUser()->getRoles(),
            'validation_groups' => 'add_post'
         ]);
         $postEdit->handleRequest($request);

         // Vérifie si le formulaire est envoyé et valide :
         if($postEdit->isSubmitted() && $postEdit->isValid()) {
            $post->setUpdatedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));

            // Insertion en bdd
            $query->persist($post);
            $query->flush();

            // Création d'un message flash
            $this->addFlash('success', 'Modification effectuée.');

            $topic = $post->getIdParent();
            // si le topic a un parent, il est donc un enfant, on récupère l'id du parent pour la redirection
            if($topic) {
               $idtopic = $topic->getId();
            } else { // sinon, on récupère simplement l'id du post en lui-même
               $idtopic = $id;
            }

            // Redirection vers la page d'accueil
            return $this->redirectToRoute('page_topic', [
               'id' => $idtopic
            ]);
         }

         // informations retournées dans la vue
         return $this->render('topic/edit.html.twig',[
            'formContent' => $postEdit->createView(),
            'categories' => $categories
         ]);
      }
   }

   /**
   * @Route("/topic/delete/{id}", name="delete_post")
   */
   public function deletePost($id): Response
   {
      $query = $this->getDoctrine()->getManager();
      $post = $query->getRepository(Post::class)->find($id);

      $topic = $post->getIdParent();
      $idtopic = $topic->getId();

      // On vérifie que le post existe bien
      if(!$post) {
         throw $this->createNotFoundException('Ce post n\'existe pas.');
      }
      
      $query = $this->getDoctrine()->getManager();
      $query->remove($post);
      $query->flush();

      // Création d'un message flash
      $this->addFlash('success', 'Message supprimé.');

      // Redirection vers la page d'accueil
      return $this->redirectToRoute('page_topic', [
         'id' => $idtopic
      ]);
   }

   /**
   * @Route("categorie/{id}/topic/add/post", name="add_new_post")
   */
   public function newTopic($id, Request $request): Response
   {
      $query = $this->getDoctrine()->getManager();
      $categories = $query->getRepository(Categorie::class)->findAll();

      // récupérer la catégorie par défaut pour le formulaire
      $categorie = $query->getRepository(Categorie::class)->find($id);
      $defaultCategorie = $categorie->getId();


      // méthode d'ajout via le formulaire
      $post = new Post();
      $formContent = $this->createForm(PostType::class, $post, [
         'role' => $this->getUser()->getRoles(),
         'validation_groups' => 'new_post'
      ]);
      $formContent->handleRequest($request);

      // Vérifie si le formulaire est envoyé et valide :
      if ($formContent->isSubmitted() && $formContent->isValid()) {
         $post->setCreatedAt(new \DateTimeImmutable("now", new \DateTimeZone('Europe/Paris')));
         $post->setUser($this->getUser());

         // Insertion en bdd
         $query->persist($post);
         $query->flush();

         // Création d'un message flash
         $this->addFlash('success', 'Votre sujet a été créé.');

         $newPost = $query->getRepository(Post::class)->findOneby([],[
            'id' => 'DESC'
         ]);

         $id = $newPost->getId();

         // Redirection vers la page d'accueil
         return $this->redirectToRoute('page_topic', [
            'id' => $id
         ]);
      }

      // informations retournées dans la vue
      return $this->render('topic/new.html.twig', [
         'formContent' => $formContent->createView(),
         'categories' => $categories
      ]);
   }
}