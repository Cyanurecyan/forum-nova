<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request; 
use Knp\Component\Pager\PaginatorInterface;

use App\Entity\Categorie;
use App\Entity\Post;

use App\Form\TopicType;

class CategorieController extends AbstractController
{
   /**
    * @Route("/categorie/{id}", name="page_categorie")
    */
   public function index($id, Request $request, PaginatorInterface $paginator): Response
   {
      $query = $this->getDoctrine()->getManager();
      $categories = $query->getRepository(Categorie::class)->findAll();

      // Récupération des données de la catégorie visée
      $categorie = $query->getRepository(Categorie::class)->findOneBy([
         'id' => $id
      ]);

      $TopicsPrio = $query->getRepository(Post::class)->findBy([
         'categorie' => $id,
         'id_parent' => null,
         'priority' => true
      ], [
         'updated_at' => 'DESC',
         'created_at' => 'DESC',
      ]);

      // Récupération des topics dans la catégorie, pour être paginés
      $rTopics = $query->getRepository(Post::class)->findBy([
         'categorie' => $id,
         'id_parent' => null,
         'priority' => null
      ], [
         'updated_at' => 'DESC',
         'created_at' => 'DESC'
      ]);

      $page = $request->query->getInt('page', 1);
      $topics = $paginator->paginate(
         $rTopics, // Requête contenant les données à paginer (ici nos articles)
         $page === 0 ? 1 : $page, // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
         10 // Nombre de résultats par page
      );

      // informations retournées dans la vue
      return $this->render('categorie/index.html.twig',[
         'categories' => $categories,
         'categorie' => $categorie,
         'topicsprio' => $TopicsPrio,
         'topics' => $topics
      ]);
   }
}