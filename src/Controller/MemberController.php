<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;

class MemberController extends AbstractController
{
   /**
    * @Route("/membres", name="user_list")
    */
   public function index(PaginatorInterface $paginator, Request $request): Response
   {
      $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

      $query = $this->getDoctrine()->getRepository(User::class)->findAll();

      $page = $request->query->getInt('page', 1);
      $users = $paginator->paginate(
         $query,
         $page === 0 ? 1 : $page,
         25
      );

      return $this->render('memberlist/index.html.twig', [
         'users' => $users,
         'categories' => $categories
      ]);
   }
}